﻿using Microsoft.Azure.ServiceBus;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Publisher
{
    class Program
    {
        private static string topic = "topic-test";

        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("----- This is a Publisher -----");
            Console.WriteLine(" ");
            Console.WriteLine(" ");

            var loop = true;

            do
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("Write your message: ");

                var message = Console.ReadLine();
                if (message == "q")
                    break;

                var serviceBusConnectionString = "";
                var topicClient = new TopicClient(serviceBusConnectionString, topic);
                var body = Encoding.UTF8.GetBytes(message);
                var busMessage = new Message(body);
                topicClient.SendAsync(busMessage).GetAwaiter().GetResult();

            } while (loop);

        }
    }
}
